package net.DeltaWings.Java.Calculator;

public class Main {

	private static Integer d = 0, e = 0, r = 0, g = -1;

	public static void main(String[] args) {
		Lib.output("Started !");
		setup();
	}

	private static void setup() {
		String[] a = Lib.readInput().split(" ");
		String f = "";
		for(String b : a) {
			Integer l = -1;
			for(String q : b.split("")) {
				if(Lib.isInteger(q)) {
					if(l == -1 || l == 1) l = 1;
					else {
						l = 1;
						f += " ";
					}
				} else {
					if (l == -1 || l == 0) l = 0;
					else {
						l = 0;
						f += " ";
					}
				}
				f += q;
			}
			f += " ";
		}
		String[] c = f.split(" ");
		if(c.length == 0 || c[0].equalsIgnoreCase("stop"));
		else if(c[0].equalsIgnoreCase("="))start(c);
		else setup();
	}

	private static void start(String[] b) {
		r = 0;
		if (b[0].equalsIgnoreCase("=")) {
			for (String c : b) {
				if (c.equalsIgnoreCase("+")) g = 0;
				else if (c.equalsIgnoreCase("-")) g = 1;
				else if (c.equalsIgnoreCase("*") || c.equalsIgnoreCase("x")) g = 2;
				else if (c.equalsIgnoreCase("/")) g = 3;
				if (Lib.isInteger(c)) {
					if (g != -1) {
						e = Integer.parseInt(c);
						calc(g);
						g = -1;
					} else d = Integer.parseInt(c);
				}
			}
			Lib.output(r);
			setup();
		}
	}

	private static void calc(Integer g) {
		if (g == 0) r += d + e;
		if (g == 1) r += d - e;
		if (g == 2) r += d * e;
		if (g == 3) r += d / e;
		//out("g : "+g+" d : "+d+" e : "+e);
		d = 0;
		e = 0;
	}
}